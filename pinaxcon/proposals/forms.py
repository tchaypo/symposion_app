import copy

from django import forms

from pinaxcon.proposals.models import TalkProposal, TutorialProposal, MiniconfProposal
from pinaxcon.proposals.models import SysAdminProposal, KernelProposal, OpenHardwareProposal
from pinaxcon.proposals.models import GamesProposal, ClsXLCAProposal, FuncProgProposal
from pinaxcon.proposals.models import OpenEdProposal, OpenGLAMProposal, FPGAProposal, DevDevProposal
from pinaxcon.proposals.models import ArtTechProposal, BioInformaticsProposal

DEFAULT_FIELDS =  [
    "title",
    "target_audience",
    "abstract",
    "private_abstract",
    "technical_requirements",
    "project",
    "project_url",
    "recording_release",
    "materials_release",
]

TALK_FORMAT_FIELDS = copy.copy(DEFAULT_FIELDS)
TALK_FORMAT_FIELDS.append("talk_format")

class ProposalForm(forms.ModelForm):

    required_css_class = 'label-required'

    def clean_description(self):
        value = self.cleaned_data["description"]
        if len(value) > 400:
            raise forms.ValidationError(
                u"The description must be less than 400 characters"
            )
        return value


class TalkProposalForm(ProposalForm):

    class Meta:
        model = TalkProposal
        fields = [
            "title",
            "target_audience",
            "abstract",
            "private_abstract",
            "technical_requirements",
            "project",
            "project_url",
            "video_url",
            "recording_release",
            "materials_release",
        ]


class TutorialProposalForm(ProposalForm):

    class Meta:
        model = TutorialProposal
        fields = [
            "title",
            "target_audience",
            "abstract",
            "private_abstract",
            "technical_requirements",
            "project",
            "project_url",
            "video_url",
            "recording_release",
            "materials_release",
        ]


class MiniconfProposalForm(ProposalForm):

    class Meta:
        model = MiniconfProposal
        fields = [
            "title",
            "abstract",
            "private_abstract",
            "technical_requirements",
            "recording_release",
            "materials_release",
        ]

class SysadminProposalForm(ProposalForm):

    class Meta:
        model = SysAdminProposal
        fields = TALK_FORMAT_FIELDS

class KernelProposalForm(ProposalForm):

    class Meta:
        model = KernelProposal
        fields = DEFAULT_FIELDS

class GamesProposalForm(ProposalForm):

    class Meta:
        model = GamesProposal
        fields = TALK_FORMAT_FIELDS

class OpenHardwareProposalForm(ProposalForm):

    class Meta:
        model = OpenHardwareProposal
        fields = TALK_FORMAT_FIELDS

class ClsXLCAProposalForm(ProposalForm):

    class Meta:
        model = ClsXLCAProposal
        fields = DEFAULT_FIELDS

class FuncProgProposalForm(ProposalForm):

    class Meta:
        model = FuncProgProposal
        fields = DEFAULT_FIELDS

class OpenEdProposalForm(ProposalForm):

    class Meta:
        model = OpenEdProposal 
        fields = DEFAULT_FIELDS

class OpenGLAMProposalForm(ProposalForm):

    class Meta:
        model = OpenGLAMProposal
        fields = DEFAULT_FIELDS

class FPGAProposalForm(ProposalForm):

    class Meta:
        model = FPGAProposal
        fields = DEFAULT_FIELDS

class DevDevProposalForm(ProposalForm):

    class Meta:
        model = DevDevProposal
        fields = DEFAULT_FIELDS

class ArtTechProposalForm(ProposalForm):

    class Meta:
        ARTTECH_FIELDS = copy.copy(DEFAULT_FIELDS)
        ARTTECH_FIELDS.remove("target_audience")
        ARTTECH_FIELDS.append("talk_format")
        ARTTECH_FIELDS.append("can_exhibit")
        ARTTECH_FIELDS.append("exhibition_requirements")

        model = ArtTechProposal
        fields = ARTTECH_FIELDS

class BioInformaticsProposalForm(ProposalForm):

    class Meta:
        model = BioInformaticsProposal
        fields = DEFAULT_FIELDS
