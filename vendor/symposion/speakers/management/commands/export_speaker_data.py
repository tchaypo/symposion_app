import csv
import os

from django.core.management.base import BaseCommand

from symposion.speakers.models import Speaker


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open(os.path.join(os.getcwd(), "speakers.csv"), "w") as csv_file:
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow(["Name", "Bio"])

            for speaker in Speaker.objects.all():
                csv_writer.writerow([
                    speaker.name,
                    speaker.biography,
                ])
