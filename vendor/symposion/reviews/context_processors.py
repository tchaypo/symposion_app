from symposion.proposals.models import ProposalSection


def reviews(request):
    sections = []
    manage_sections = {}
    for section in ProposalSection.objects.all():
        if request.user.has_perm("reviews.can_review_%s" % section.section.slug):
            sections.append(section)
        if request.user.has_perm("reviews.can_manage_%s" % section.section.slug):
            manage_sections.setdefault(section, []).append
    return {
        "review_sections": sections,
        "manage_sections": manage_sections
    }

